import { createApp } from "vue";
import App from "./App.vue";
import '@/permission'; // permission control

import './style/anStyle.scss';

import router from './router';
import store from './store';
import axios from './plugins/axios';

import installElementPlus from './plugins/element';
import svgIcon from '@/icons/index'; // icon
import * as api from "@/api/data";
import * as auth from "@/utils/auth";

const app = createApp(App);

installElementPlus(app);

app.config.globalProperties.API = api;
app.config.globalProperties.AUTH = auth;

app.use(router);
app.use(store);
app.use(axios);
app.use(svgIcon);
app.mount("#app");