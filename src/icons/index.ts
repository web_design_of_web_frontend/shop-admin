
import svgIcon from "@/components/SvgIcon/index.vue";


const requireAll = (requireContext: any) =>
  requireContext.keys().map(requireContext);
const req = require.context("./svg", false, /\.svg$/);

requireAll(req);

const icons = {
  install: function (app: any) {
    app.component('svg-icon', svgIcon)
  }
}
export default icons