import { createRouter, createWebHashHistory } from 'vue-router'
const Layout = () => import('@/components/Layout/Layout.vue')

const routes = [
  {
    path: '/',
    redirect: '/home',
    component: Layout,
    children: [{
      path: '/home',
      component: () => import('@/views/home/index.vue'),
      meta: { title: '首页', icon: 'home' },
    }]
  }, {
    path: '/goods',
    component: Layout,
    meta: { title: '商品管理', icon: 'goods' },
    children: [
      {
        path: '/goods',
        component: () => import('@/views/goods/index.vue'),
        meta: { title: '商品列表' }
      }, {
        path: '/goodsType',
        component: () => import('@/views/goods/goodsType.vue'),
        meta: { title: '分类管理' }
      }, {
        path: '/goodsSpec',
        component: () => import('@/views/goods/goodsSpec.vue'),
        meta: { title: '规格属性' }
      }, {
        path: '/goodsAdd',
        props: (route: any) => ({ id: route.query.id }),
        component: () => import('@/views/goods/goodsAdd.vue'),
        meta: { title: '新增商品' }
      }
    ]
  }, {
    path: '/order',
    component: Layout,
    meta: { title: '订单管理', icon: 'order' },
    children: [
      {
        path: '/order',
        props: (route: any) => ({ tab: route.query.tab }),
        component: () => import('@/views/order/order.vue'),
        meta: { title: '订单管理', icon: 'order' }
      }
    ]
  }, {
    path: '/sys',
    component: Layout,
    meta: { title: '系统设置', icon: 'setting' },
    children: [
      {
        path: '/sys/home',
        component: () => import('@/views/sys/index.vue'),
        meta: { title: '首页设置' }
      }, {
        path: '/sys/freight',
        component: () => import('@/views/sys/freight.vue'),
        meta: { title: '运费设置' }
      },
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/login/index.vue"),
    hidden: true
  }, {
    path: '/other',
    component: Layout,
    hidden: true,
    children: [{
      path: "/orderInfo",
      component: () => import("@/views/order/orderInfo.vue"),
      props: (route: any) => ({ id: route.query.id }),
      meta: { title: '订单详情' },
    }]
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})



export default router
