/* 特殊字符*/
export function isvalidUsername(str: String) {
  const regName = /[~#^$@%&!*()<>:;'"{}【】 {2}]/;
  return regName.test(str.trim());
}

/* 合法uri*/
export function validateURL(textval: string) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return urlregex.test(textval);
}

/* 小写字母*/
export function validateLowerCase(str: string) {
  const reg = /^[a-z]+$/;
  return reg.test(str);
}

/* 大写字母*/
export function validateUpperCase(str: string) {
  const reg = /^[A-Z]+$/;
  return reg.test(str);
}

/* 大小写字母*/
export function validatAlphabets(str: string) {
  const reg = /^[A-Za-z]+$/;
  return reg.test(str);
}

/* 数字*/
export function validatNum(str: string) {
  const reg = /^[0-9]+$/;
  return reg.test(str);
}

/* 手机号*/
export function validatPhone(str: string) {
  const reg = /^1[3456789]\d{9}$/;
  return reg.test(str);
}

/* 邮箱 */
export function validaEmail(val: string) {
  let reg = /[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
  return reg.test(val);
}

/*  是否为正整数 */
export function isPositiveInteger(s: string) {
  let re = /^[0-9]+$/;
  return re.test(s);
}

/*  是否为传真 格式 区号-电话(固话)/传真号码-分机号*/
export function isFax(s: string) {
  let re = /^(0?\d{2,3}\-)?[1-9]\d{6,7}(\-\d{1,4})?$/;
  return re.test(s);
}

/* 必须包含字母和数字 */
export function isPassword(str: string) {
  const reg = /(?!^\d+$)(?!^[a-zA-Z]+$)[0-9a-zA-Z]{4,23}/
  return reg.test(str)
}

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path: string) {
  return /^(https?:|mailto:|tel:)/.test(path);
}