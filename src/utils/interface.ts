export interface Goods {
  id: Number,
  name: string,   // 商品名称
  image: string,  // 预览图
  price: Number,  // 商品单价
  sellCount: Number, // 销售数量
  state: Number, // 状态 1上架 0下架
  stock: Number, // 库存
  typeId: Number, // 分类ID
  createTime: string,
  freightBase: Number, // 运费基数
  options: string
}

export interface GoodsInfo extends Goods {
  goodsId: Number,
  mainPic: string, // 主图
  detail: string // 详情
}

// 商品分类
export interface GoodsType {
  id: Number,
  typeName: string,
  parentID: Number
}

// 商品规格
export interface goodsOption {
  id: number,
  name: string,
  price: number,
  stock: number,
  goodsSpecID: number
}

// 商品规格列表数据
export interface GoodsSpec {
  id: number,
  name: string
}

// 运费表
export interface Freight {
  id: number,
  province: string,
  freight: number,
  areaID: number
}

// 订单表
export interface Order {
  id: number,
  userID: number,
  totalMoney: number, // 订单总金额
  freight: number, // 运费
  createTime: string,
  state: number, // 订单状态：1000待付款，1100待发货，1110待收货，1120待评价，2000交易完成；百位
  consignee: string, // 收件人
  phone: string,
  address: string,
  remarks: string, // 订单备注
  customerNotes: string, // 用户自定义备注 留言
  payTime: string, // 付款时间
  deliveryTime: string, // 发货时间
  confirmTime: string, // 确认时间
  completeTime: string, // 交易完成时间
  isDelete: number, // 是否被删除（1被用户删除）
}

// 订单详情
export interface OrderInfo extends Order {
  orderDetails: Array<OrderDetails>
}

// 订单详情
export interface OrderDetails {
  id: number,
  orderID: number,
  goodsID: number,
  goodsNum: number,
  money: number,  // 商品单价
  goodsStandard: string, // 商品规格
  goods: Goods
}