import store from '@/store';
import { GoodsType } from "@/utils/interface";

// 数组去重
export function uniqueArray(val: Array<any>) {
  return val.filter((v, i) => val.indexOf(v) === i);
}

// 数组对象去重
export function uniqueArrayObg(arr: Array<any>) {
  let result = [];
  let obj: Map<string, Boolean> = new Map<string, Boolean>();
  for (var i = 0; i < arr.length; i++) {
    if (!obj.get(arr[i].id)) {
      result.push(arr[i]);
      obj.set(arr[i].id, true)
    }
  }
  return result
}

/* 生成验证码的函数  length验证码长度  type 验证码类型 */
export function createCode(length: Number, type: String) {
  let code = "";
  let selectChar: (number | string)[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  let numberChar = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  for (let i = 0; i < length; i++) {
    if (type === 'number') {
      let charIndex = Math.floor(Math.random() * 9);
      code += numberChar[charIndex];
    } else {
      let charIndex = Math.floor(Math.random() * 61);
      code += selectChar[charIndex];
    }
  }
  return code;
}

/* 对象是否为空 */
export function isEmpty(obj: any) {
  if (typeof obj == 'number') {
    obj = obj.toString();
  }
  // 本身为空直接返回true
  if (obj == null) return true;
  // 然后可以根据长度判断，在低版本的ie浏览器中无法这样判断。
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  // 最后通过属性长度判断。
  for (let key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

// 保留两位小数 自动补零
export function returnFloat(num: string): string {
  let value: number = Math.round(parseFloat(num) * 100) / 100;
  let xsd = value.toString().split(".");
  let nd: string = value.toString();
  if (xsd.length == 1) {
    nd = value.toString() + ".00";
    return nd;
  }
  if (xsd.length > 1) {
    if (xsd[1].length < 2) {
      nd = value.toString() + "0";
    }
  }
  return nd
}

/**
 * 输入平级数组 返回树状结构数组 
 * @param {Array} od
 * @param {string} key // 主id
 * @param {string} parentKey // 父级字段名称
 * @returns {Array}  返回数组
 */
export function createTree(od: [], key: string, parentKey: string): Array<any> {
  let newary: Array<any> = []
  function recursion(arry: [], id: any): Array<any> {
    let nd: Array<any> = []
    arry.forEach(item => {
      if (item[parentKey] == id) {
        nd.push(item)
      }
    })
    // 开始递归
    nd.forEach(item => {
      if (recursion(od, item[key]).length > 0) {
        item.children = recursion(od, item[key])
      }
    })
    return nd
  }
  // 第一次循环 取一级菜单
  od.forEach(item => {
    if (!item[parentKey]) {
      newary.push(item)
    }
  })
  // 第二次循环 取二级菜单
  newary.forEach(item => {
    if (recursion(od, item[key]).length > 0) {
      item.children = recursion(od, item[key])
    }
  })
  return newary
}

/**
 * 商品分类
 * @param {number} id 分类ID
 * @param {Array} list 分类列表
 * @returns {Array}  返回所属分类名称
 */
export function getTypeName(id: number, list: Array<GoodsType>) {
  let name = ''
  // 查询商品分类
  for (let i = 0; i < list.length; i++) {
    if (list[i].id == id) {
      name = list[i].typeName;
    }
  }
  return name;
}

// 订单状态
export function filterState(state: number) {
  let nd = "未知"
  switch (state) {
    case 1000:
      nd = "待付款";
      break;
    case 1100:
      nd = "待发货";
      break;
    case 1110:
      nd = "待收货";
      break;
    case 1120:
      nd = "待评价";
      break;
    case 2000:
      nd = "交易完成";
      break;
  }
  if (state < 0) {
    nd = "交易关闭"
  }
  return nd;
}

// 时间格式转换
// changeTime("yyyy-MM-dd HH:mm:ss", new Date())
export function changeTime(fmt: string, data: Date) {
  let netime = new Date();
  if (data) {
    netime = data
  }
  let o: any = {
    'M+': netime.getMonth() + 1, // 月份
    'd+': netime.getDate(), // 日
    'H+': netime.getHours(), // 小时
    'm+': netime.getMinutes(), // 分
    's+': netime.getSeconds(), // 秒
    'q+': Math.floor((netime.getMonth() + 3) / 3), // 季度
    'S': netime.getMilliseconds() // 毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (netime.getFullYear() + '').substr(4 - RegExp.$1.length));
  for (let k in o) { if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length)); }
  return fmt;
}

// 换行符转换
export function changeEnter(str: string) {
  //替换所有的换行符
  let nd = str.replace(/\r\n/g, "<br>");
  nd = nd.replace(/\n/g, "<br>");
  nd = nd.replace(/(\r\n)|(\n)/g, '<br>');
  //替换所有的空格（中文空格、英文空格都会被替换）
  nd = nd.replace(/\s/g, " ");
  return nd
}