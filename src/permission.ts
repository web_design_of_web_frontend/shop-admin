import router from './router';
import store from './store';
import { ElMessage } from 'element-plus';

const whiteList = ['/login']; // 不重定向白名单
router.beforeEach((to, from, next) => {
  if (store.state.userModule.username) {
    if (to.path === '/login') {
      next({ path: '/' });
    } else {
      if (!store.state.userModule.username) {
        store.dispatch('userModule/logOut').then(() => {
          ElMessage.error('您已退出，请重新登录！');
          next({ path: '/' });
        });
      } else {
        next();
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next();
    } else {
      next('/login');
    }
  }
});

router.afterEach(() => {
});
