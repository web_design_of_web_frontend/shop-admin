
export default interface userInfo {
  id: number;
  username: String;
  phone: String;
  userPhoto: String;
  grade: number;
  [propName: string]: any;
}