
import { GoodsType, GoodsSpec } from "@/utils/interface"
export default interface AppInterface {
  goodsType: Array<GoodsType>;
  goodsSpec: Array<GoodsSpec>;
}