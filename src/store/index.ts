
import { InjectionKey } from "vue";
import {
  createStore,
  createLogger,
  Store,
  useStore as baseUseStore
} from "vuex";
import RootStateTypes from "@/store/interface";
import RootStates from "@/store/state";

// 导入模块
import userModule from "@/store/modules/user/user";
import appModule from "@/store/modules/app/app";

const debug = process.env.NODE_ENV !== "production";
export default createStore<RootStateTypes>({
  state: RootStates,
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    userModule,
    appModule
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
});

export const key: InjectionKey<Store<RootStateTypes>> = Symbol("vue-store");
export function useStore() {
  return baseUseStore(key);
}