
// 子模块 sate类型引入
import userStateTypes from "@/store/modules/user/interface";
import appStateTypes from "@/store/modules/app/interface";

// root级 sate 类型定义
export default interface RootStateTypes {
  appModule: any;
  userModule: any;
  testData: string;
}

// vuex 所有sate类型定义集成
export interface AllSateTypes extends RootStateTypes {
  appModule: appStateTypes;
  testModule: userStateTypes;
}