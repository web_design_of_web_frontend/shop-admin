import { request } from '../plugins/axios'

const qs = require('qs');

// 登录
export function login(d: Object) {
  return request({
    url: '/manage/user/login',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 修改密码
export function userUpdate(d: Object) {
  return request({
    url: '/manage/user/update',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 查询商品列表
export function goodsList(d: Object) {
  return request({
    url: '/manage/goods/list',
    method: 'get',
    params: d
  });
}

// 修改商品
export function goodsUpdate(d: Object) {
  return request({
    url: '/manage/goods/insert',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 查询商品详情
export function goodsInfo(id: String) {
  return request({
    url: `/manage/goods/${id}`,
    method: 'get',
    params: ''
  });
}

// 删除商品
export function goodsDelete(ids: String) {
  return request({
    url: `/manage/goods`,
    method: 'delete',
    params: { ids: ids }
  });
}

// 查询分类列表
export function goodsTypeList() {
  return request({
    url: `/manage/goodsType/list`,
    method: 'get',
    params: ''
  });
}

// 新增或修改商品分类
export function goodsTypeAdd(d: Object) {
  return request({
    url: '/manage/goodsType/add',
    method: 'post',
    data: qs.stringify(d)
  });
}

// 商品分类 删除 
export function goodsTypeDelete(id: Number) {
  return request({
    url: `/manage/goodsType/delete/${id}`,
    method: 'get',
    params: ''
  });
}

// 查询商品规格
export function goodsSpecList() {
  return request({
    url: `/manage/goodsSpec`,
    method: 'get',
    params: ''
  });
}

// 新增或修改商品规格
export function goodsSpecUpdate(d: Object) {
  return request({
    url: `/manage/goodsSpec`,
    method: 'post',
    data: qs.stringify(d)
  });
}

// 删除商品规格
export function goodsSpecDelete(id: number) {
  return request({
    url: `/manage/goodsSpec`,
    method: 'delete',
    params: { id: id }
  });
}

// 促销商品接口 查询
export function indexSales() {
  return request({
    url: `/manage/indexSales`,
    method: 'get',
    params: ''
  });
}

// 促销商品接口 新增或修改
export function indexSalesUpdate(d: any) {
  return request({
    url: `/manage/indexSales`,
    method: 'post',
    data: qs.stringify(d)
  });
}

// 促销商品接口 删除
export function indexSalesDelete(id: number) {
  return request({
    url: `/manage/indexSales`,
    method: 'delete',
    params: { id: id }
  });
}

// 运费查询
export function getFreight() {
  return request({
    url: `/manage/freight`,
    method: 'get',
    params: ''
  });
}

// 修改或新增运费
export function postFreight(d: any) {
  return request({
    url: `/manage/freight`,
    method: 'post',
    data: qs.stringify(d)
  });
}

// 删除运费
export function deleteFreight(ids: String) {
  return request({
    url: `/manage/freight`,
    method: 'delete',
    params: { ids: ids }
  });
}

// 查询订单列表
export function orderList(d: any) {
  return request({
    url: `/manage/order/list`,
    method: 'get',
    params: d
  });
}

// 订单详情
export function orderInfo(id: number) {
  return request({
    url: `/manage/order/${id}`,
    method: 'get',
    params: ''
  });
}

// 修改订单
export function postOrder(d: any) {
  return request({
    url: `/manage/order/update`,
    method: 'post',
    data: qs.stringify(d)
  });
}

// 删除订单
export function deleteOrder(ids: String) {
  return request({
    url: `/manage/order`,
    method: 'delete',
    params: { ids: ids }
  });
}

// 发货
export function postExpress(d: any) {
  return request({
    url: `/manage/order/updateExpress`,
    method: 'post',
    data: qs.stringify(d)
  });
}

// 删除快递信息
export function deleteExpress(id: number) {
  return request({
    url: `/manage/order/updateExpress/delete`,
    method: 'post',
    data: qs.stringify({ id: id })
  });
}

// 销售情况统计
export function statisticsSale() {
  return request({
    url: `/manage/statistics/sale`,
    method: 'get',
    params: ''
  });
}