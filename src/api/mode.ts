
/* 静态数据 */

interface ObjectType {
  [key: string]: string;
}

// 快递公司以及编号  测试数据
export const expressCode: ObjectType = {
  1: "顺丰快递",
  2: "韵达快递",
}
