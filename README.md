# vue3
>vue3  @vue/cli4.5 + typescript + element-plus

## Project setup
```
npm install
```

## 某个模块安装失败
```
npm i node-sass --sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


